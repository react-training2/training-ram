import React from 'react';

const Box = (WrappedComponent) =>{
    return(props) => {
        return(
            <WrappedComponent {...props} />
        )
    }
    
}

export default Box; 