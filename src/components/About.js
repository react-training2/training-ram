import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const About = () =>{

    const [cartDetails, setCartDetails] = useState({
        item: 'iphone',
        cost: '$500',
        qty: 50
    })
    return(
        <div>
            Inside About Component
            <div>
                <Link to={{
                    pathname:'/contact',
                    state: cartDetails
                }} >
                    checkout
                 </Link>

            </div>
        </div>
    )
}

export default About;