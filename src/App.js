import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Confirmation from './components/Confirmation';



const App = () => {
  return(
    <div className="container">
      <Router>
        <Switch>
          <Route path='/home' exact component={Home} />
          <Route path='/aboutus' component={About} />
          <Route path='/contact' component={Contact} />
          <Route path='/confirmation' component={Confirmation} />
        </Switch>
      </Router>
    </div>
  )
  
};

export default App;
